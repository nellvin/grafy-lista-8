package graphs;

import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.parse.Parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static guru.nidi.graphviz.model.Factory.node;

public class DirectedGraph {
	
	// liczba wierzcholkow
	private int v;

	// liczba krawedzi
	private int e;

	// listy sasiedztwa
	private List<DirectedEdge>[] neighborhoodLists;

	@SuppressWarnings("unchecked")
	public DirectedGraph(int v) {

		this.v = v;
		this.e = 0;
		this.neighborhoodLists = (List<DirectedEdge>[]) new List[v];
		for (int i = 0; i < v; i++) {
			neighborhoodLists[i] = new ArrayList<DirectedEdge>();
		}

	}

	public int getNumberOfEdges() {
		return e;
	}

	public int getNumberOfVertices() {
		return v;
	}

	public void addEdge(DirectedEdge edge) {
		neighborhoodLists[edge.from()].add(edge);
		e++;
	}

	public Iterable<DirectedEdge> getNeighborhoodList(int v) {
		return neighborhoodLists[v];
	}

	public StringBuilder showGraph() throws IOException {

		List<DirectedEdge> list;
		StringBuilder stringBuilder = new StringBuilder("digraph {\n");
		for (int i = 0; i < neighborhoodLists.length; i++) {
			list = neighborhoodLists[i];

			DirectedEdge edge;
			for (int j = 0; j < list.size(); j++) {
				edge = list.get(j);
				stringBuilder.append("\"" + edge.from() + "\""); //from
				stringBuilder.append(" -> "); //arrow
				stringBuilder.append("\"" + edge.to() + "\""); //to
				stringBuilder.append(" [\"label\"=\"" + edge.getWeight() + "\"]\n"); //to
			}
		}
		stringBuilder.append("}");
		System.out.println(stringBuilder.toString());
		MutableGraph graph = Parser.read(stringBuilder.toString());
		System.out.println(graph.toString());
		Graphviz.fromGraph(graph).render(Format.PNG).toFile(new File("example/ex0.png"));
		return stringBuilder;
	}

	public void printPath(Iterable<DirectedEdge> path) throws IOException {
		Iterator<DirectedEdge> iterator = path.iterator();

		ArrayList<DirectedEdge> arrayListPath = new ArrayList<>();
		while (iterator.hasNext()) {
			arrayListPath.add(iterator.next());
		}

		List<DirectedEdge> list;
		StringBuilder stringBuilder = new StringBuilder("digraph {\n");
		for (int i = 0; i < neighborhoodLists.length; i++) {
			list = neighborhoodLists[i];

			DirectedEdge edge;
			for (int j = 0; j < list.size(); j++) {
				edge = list.get(j);
				stringBuilder.append("\"" + edge.from() + "\""); //from
				stringBuilder.append(" -> "); //arrow
				stringBuilder.append("\"" + edge.to() + "\""); //to
				stringBuilder.append(" [\"label\"=\"" + edge.getWeight() + "\" \"color\"=\""
						+ (arrayListPath.contains(edge) ? "red" : "black") + "\"]\n"); //to
			}
		}
		stringBuilder.append("}");
		System.out.println(stringBuilder.toString());
		MutableGraph graph = Parser.read(stringBuilder.toString());
		System.out.println(graph.toString());
		Graphviz.fromGraph(graph).render(Format.PNG).toFile(new File("example/ex1.png"));
	}
}