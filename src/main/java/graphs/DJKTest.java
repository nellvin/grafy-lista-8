package graphs;

import java.io.IOException;
import java.util.Iterator;

public class DJKTest {

	public static void main(String[] args) throws IOException {

		DirectedGraph graph = new DirectedGraph(7);

		graph.addEdge(new DirectedEdge(6, 1, 2));
		graph.addEdge(new DirectedEdge(0, 2, 3));
		graph.addEdge(new DirectedEdge(0, 3, 7));
		graph.addEdge(new DirectedEdge(1, 3, 1));
		graph.addEdge(new DirectedEdge(1, 4, 1));
		graph.addEdge(new DirectedEdge(2, 3, 3));
		graph.addEdge(new DirectedEdge(2, 4, 5));
		graph.addEdge(new DirectedEdge(3, 3, 2));
		graph.addEdge(new DirectedEdge(3, 5, 2));
		graph.addEdge(new DirectedEdge(3, 6, 7));
		graph.addEdge(new DirectedEdge(4, 3, 2));
		graph.addEdge(new DirectedEdge(5, 6, 4));
		graph.showGraph();

		int source = 0;

		DijkstraShortestPath shortestPath = new DijkstraShortestPath(graph, source);

		Iterable<DirectedEdge> path = shortestPath.getPathTo(6);
		Iterator<DirectedEdge> iterator = path.iterator();


		graph.printPath(path);

	}

}
