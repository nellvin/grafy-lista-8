package graphs;


public class GraphTest {
    public static void main(String[] args) {
        Graph g = new GraphAM();
        g.addVertex(5);
        g.addVertex(7);
        g.addEdge(5, 7);
        g.changeValueOfEdge(5, 7, 2);
        g.printGraph();
        g.write();


        Graph a = new GraphAL();
        a.addVertex(5);
        a.addVertex(2);
        a.addVertex(8);
        a.addVertex(7);
        a.addEdge(5, 7);
        a.addEdge(5, 2);
        a.addEdge(5, 8);
        a.addEdge(3, 8);
        a.addEdge(8, 2);
        a.addEdge(2, 8);
        a.changeValueOfEdge(5, 7, 2);
        a.printGraph();
//        a.write();
    }

}
